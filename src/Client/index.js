const axios = require('axios')
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = 3000;
const loggerCall = 'http://localhost:3003/log'
const courierHost = 'http://localhost:3001/'
const restaurantHost = 'http://localhost:3002/'

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(express.json());

app.listen(port, function(err){
    if(err) console.log(err);
    console.log("Client listening on port ",port)
});

// Create new order
app.post('/create_order', function(req,res){
    var orderNum = Math.floor(Math.random()*(9999-100+1)+100)
    var description = "FROM CLIENT - Order "+orderNum+" was created"
    axios.post(loggerCall,{'description': description})
    res.json({order : orderNum}) 
});

// Verify Status on restaurant
app.get('/get_order_status_r/:id', function(req,res){
    var orderNum = req.params.id
    var description = "FROM CLIENT - Request to get order status -> No."+orderNum+" from restaurant"
    //axios.get(restaurantHost+'get_order_status/'+orderNum)
    axios.post(loggerCall,{'description': description})
    res.send(description)
});

// Verify Status on courier
app.get('/get_order_status_c', function(req,res){
    var orderNum = req.body.id
    var description = "FROM CLIENT - Request to get order status -> No."+orderNum+" from courier"
    axios.get(courierHost+'get_order_status/'+orderNum)
    axios.post(loggerCall,{'description': description})
    res.send(description)
});