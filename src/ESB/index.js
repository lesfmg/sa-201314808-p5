const axios = require('axios')
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = 3004;

const clientHost = 'http://localhost:3000/'
const courierHost = 'http://localhost:3001/'
const restaurantHost = 'http://localhost:3002/'

const loggerCall = 'http://localhost:3003/log'

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(express.json());

app.listen(port, function(err){
    if(err) console.log(err);
    console.log("ESB listening on port ",port)
});

// ------------------------------------------------------------------------------------------
//                                      CLIENT ENDPOINTS
// ------------------------------------------------------------------------------------------

// Create new order
app.post('/client/create_order', function(req,res){
    axios.post(loggerCall,{'description': 'FROM ESB -  client is creating a new order'})
    axios.post(clientHost+'create_order')
        .then(function(response){
            var orderNum = response['data']['order']
            axios.post(restaurantHost+'place_order/'+orderNum)
            axios.post(loggerCall,{'description': 'FROM ESB -  Created the order for the client and posted it on the restaurant'})
        })
        .catch(function(error){
            axios.post(loggerCall,{'description': 'FROM ESB - Error when the client sends the request - '+error})
        })
    res.send('client completed the order correctly')
});


// Verify Status on restaurant
app.get('/client/get_order_status_r', function(req,res){
    axios.post(loggerCall,{'description': 'FROM ESB - (call) client is consulting order status with the restaurant'})
    var orderNum = req.body.id
    axios.get(restaurantHost+'get_order_status/:'+orderNum)
        .then(function(response){
            var orderStatus = response['data']['OStatus']
            axios.post(loggerCall,{'description': 'FROM ESB - resolved the order status from the restaurant - '+orderStatus})
        })
        .catch(function(error){
            axios.post(loggerCall,{'description': 'FROM ESB - Error when the consults the status with the restaurant - '+error})
        })
    res.send('Order status resolved from the restaurant')
});

// Verify Status on courier
app.get('/client/get_order_status_c', function(req,res){
    axios.post(loggerCall,{'description': 'FROM ESB -  (call) client is consulting order status with the courier'})
    var orderNum = req.body.id
    axios.get(courierHost+'get_order_status/:'+orderNum)
        .then(function(response){
            var orderStatus = response['data']['OStatus']
            axios.post(loggerCall,{'description': 'FROM ESB - resolved the order status from the courier - '+orderStatus})            
        })
        .catch(function(error){
            axios.post(loggerCall,{'description': 'FROM ESB - Error when the consults the status with the courier - '+error})
        })
    res.send('Order status resolved from the courier')
});


// ------------------------------------------------------------------------------------------
//                                      RESTAURANT ENDPOINTS
// ------------------------------------------------------------------------------------------

app.post('/restaurant/post_order_ready', function(req,res){
    axios.post(loggerCall,{'description': 'FROM ESB - (call) Restaurant is alerting the courier about the order ready'})
    var orderNum = req.body.id

    axios.post(restaurantHost+'post_order_ready/:'+orderNum)
        .then(function(){
            axios.post(loggerCall,{'description': 'FROM ESB - restaurant sent the message successfully'}) 
        })
        .catch(function(error){
            axios.post(loggerCall,{'description': 'FROM ESB - Error when the restaurant was alerting the courier about the order ready - '+error})
        })

    axios.post(courierHost+'post_order_ready/:'+orderNum)
        .then(function(){
            axios.post(loggerCall,{'description': 'FROM ESB - Courrier has receibed the ready message successfully'}) 
        })
        .catch(function(error){
            axios.post(loggerCall,{'description': 'FROM ESB - Error when the courier was receibing the message about the order ready - '+error})
        })

    res.send('The restaurant has alerted the courier')
});


// ------------------------------------------------------------------------------------------
//                                      COURIER ENDPOINTS
// ------------------------------------------------------------------------------------------

// Mark as finished
app.post('/courier/post_delivered', function(req,res){
    axios.post(loggerCall,{'description': 'FROM ESB - (call) Courier is defining the order as delivered'})
    var orderNum = req.body.id
    axios.post(courierHost+'post_delivered/:'+orderNum)
        .then(function(){
            axios.post(loggerCall,{'description': 'FROM ESB - courier marked as delivered succesfully'}) 
        })
        .catch(function(error){
            axios.post(loggerCall,{'description': 'FROM ESB - Error when the courier marked as delivered succesfully - '+error})
        })
    res.send('The courier marked as delivered succesfully')
});