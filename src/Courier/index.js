const axios = require('axios')
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = 3001;
const loggerCall = 'http://localhost:3003/log'

const options = ['waiting in the restaurant','on the way','delivered'];

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(express.json());

app.listen(port, function(err){
    if(err) console.log(err);
    console.log("Courier listening on port ",port)
});

// Receive order from restaurant
app.post('/post_order_ready/:id', function(req,res){
    var orderNum = req.params.id
    var description = "FROM COURIER - The order No."+orderNum+" has been received"
    axios.post(loggerCall,{'description': description})
    res.send(description)
});

// Inform order status
app.get('/get_order_status/:id', function(req,res){
    var orderNum = req.params.id
    var num = Math.floor(Math.random()*(2-0+1)+0)
    var status = options[num]
    var description = "FROM COURIER - The order No."+orderNum+" is now on "+status+" status"
    axios.post(loggerCall,{'description': description})
    res.json({OStatus : status}) 
});

// Mark as finished
app.post('/post_delivered/:id', function(req,res){
    var orderNum = req.params.id
    var description = "FROM COURIER - The order No."+orderNum+" has been delivered"
    axios.post(loggerCall,{'description': description})
    res.send(description)
});