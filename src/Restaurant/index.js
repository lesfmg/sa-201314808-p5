const axios = require('axios')
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = 3002;
const loggerCall = 'http://localhost:3003/log'
const courierHost = 'http://localhost:3001/'

const options = ['cooking','waiting for courier','sent'];

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(express.json());

app.listen(port, function(err){
    if(err) console.log(err);
    console.log("Restaurant listening on port ",port)
});

// Receive order from client
app.post('/place_order/:id', function(req,res){
    var orderNum = req.params.id
    var description = "FROM RESTAURANT - The order No."+orderNum+" has been received"
    axios.post(loggerCall,{'description': description})
    res.send(orderNum)
});

// Inform order status
app.get('/get_order_status/:id', function(req,res){
    var orderNum = req.params.id
    var num = Math.floor(Math.random()*(2-0+1)+0)
    var status = options[num]
    var description = "FROM RESTAURANT - The order No."+orderNum+" is now on "+status+" status"
    axios.post(loggerCall,{'description': description})
    res.json({OStatus : status}) 
});

// Alert the courier that the order is ready
app.post('/post_order_ready/:id', function(req,res){
    var orderNum = req.params.id
    var description = "FROM RESTAURANT - Order "+orderNum+" is ready and courier alerted"
    //axios.post(courierHost+'post_order_ready/'+orderNum)
    axios.post(loggerCall,{'description': description})
    res.send(description)
});