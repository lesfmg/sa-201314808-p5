const {createLogger, format, transports} = require('winston')

module.exports = createLogger({
    format: format.combine(
        format.timestamp({
            format: 'MMM dd, YYYY - hh:mm:ss a'
        }), 
        format.simple(),
        format.printf(info => `log: [${info.timestamp}] - ${info.level} -> ${info.message}`)
        ),
    transports: [
        new transports.File({
            filename: `server.log`,
            
        })
    ]
});