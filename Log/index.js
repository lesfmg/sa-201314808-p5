const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const logger = require('../Log/logger');

const port = 3003;
app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(express.json());

app.listen(port, function(err){
    if(err) console.log(err);
    console.log("Loogger listening on port ",port)
});


// get Log
app.post('/log', function(req,res){
    var description = req.body.description
    logger.info(description)
    res.send('completed')
});